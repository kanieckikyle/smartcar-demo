interface Car {
  id: number;
  owner: Owner;
  make: string;
  model: string;
  year: number;
  color: string;
}
