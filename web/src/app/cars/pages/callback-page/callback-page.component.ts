import { Component, OnInit } from '@angular/core';
import { CarService } from '../../services/car.service';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-callback-page',
  templateUrl: './callback-page.component.html',
  styleUrls: ['./callback-page.component.scss']
})
export class CallbackPageComponent implements OnInit {

  constructor(private carService: CarService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.queryParamMap
      .pipe(
        map((params: ParamMap) => params.get('code'))
      )
      .subscribe((code: string) => {
        this.carService.sendPaypalInfo({code: code});
      });
  }

}
