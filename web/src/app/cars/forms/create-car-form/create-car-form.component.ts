import { Component, OnInit, ViewChild } from '@angular/core';
import { CarService } from '../../services/car.service';
import { map, switchMap } from 'rxjs/operators';
import { MatStepper } from '@angular/material/stepper';
import { FormArray, Validators, FormControl, AbstractControl } from '@angular/forms';

@Component({
  selector: 'app-create-car-form',
  templateUrl: './create-car-form.component.html',
  styleUrls: ['./create-car-form.component.scss']
})
export class CreateCarFormComponent implements OnInit {

  signedIntoSmartcar: boolean = false;
  private oAuthUrl: string;
  newCars: Car[] = [];

  @ViewChild('stepper') stepper: MatStepper;

  form: FormArray = new FormArray([]);

  constructor(private carService: CarService) { }

  ngOnInit() {
    this.carService.getOauthUrl()
    .subscribe((url: string) => {
      this.oAuthUrl = url;
    })
  }

  signinToSmartcar() {
    this.carService.openPaypalWindow(this.oAuthUrl)
      .pipe(
        map((info: any) => info.code),
        switchMap((code: string) => {
          this.carService.closePaypalWindow();
          return this.carService.exchangeCode(code);
        })
      )
      .subscribe((cars: Car[]) => {
        this.signedIntoSmartcar = true;
        this.newCars = cars;
        cars.forEach((_, index) => {
          this.form.insert(index, new FormControl('', [
            Validators.required,
            Validators.maxLength(15)
          ]));
        });
        setTimeout(() => this.stepper.next(), 100);
      });
  }

  submitCarColors() {
    const obs = [];
    this.newCars.forEach((car, index) => {
      const data = Object.assign({}, car);
      data['color'] = this.form.at(index).value;
      obs.push(this.carService.updateCar(car.id, data));
    });

    forkJoin(obs)
      .subscribe((result: Car[]) => {

      });
  }

  getFormControl(index: number): AbstractControl {
    return this.form.at(index);
  }

}
