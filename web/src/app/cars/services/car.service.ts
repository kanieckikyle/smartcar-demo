import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError as observableThrowError, Subject, interval, merge, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { map, take, filter, switchMap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CarService {

  popupWindow: Window;
  protected gotNewInfo = new Subject<boolean>();
  closeChecker: Observable<any>;

  constructor(private http: HttpClient) { }

  getCars(): Observable<Car[]> {
    return this.http
      .get(`${environment.baseUrl}/cars/`)
      .pipe(
        map((result: Car[]) => result)
      );
  }

  getCar(id: number): Observable<Car> {
    return this.http
      .get(`${environment.baseUrl}/cars/${id}/`)
      .pipe(
        map((result: Car) => result)
      )
  }

  updateCar(id: number, data: any) {
    return this.http
      .patch(`${environment.baseUrl}/cars/${id}/`, data);
  }

  getOauthUrl(): Observable<string> {
    return this.http
      .get(`${environment.baseUrl}/cars/authorize/`)
      .pipe(
        map((result: any) => result.redirect_url)
      );
  }

  exchangeCode(code: string): Observable<Car[]> {
    return this.http
      .post<Car[]>(`${environment.baseUrl}/cars/authorize/`, {code: code});
  }

  /**
   * Opens a paypal window to the corresponding window and looks for the paypal window to be closed unexpectedly
   * @throws PopupsBlockedError
   * @throws PaypalWindowClosedError
   * @returns data: any
   */
  openPaypalWindow(url: string): Observable<any> {
    // open the paypal window
    this.popupWindow = window.open(url, '_blank', this.getPopupWindowKwargsString(), true);

    if (this.popupsAreBlocked()) {
      this.handlePopupsBlocked();
      return observableThrowError(new Error());
    }

    window.addEventListener('message', event => this.getPaypalWindowData(event), false);

    // I have to do this with the popup since most browsers are shitty about the host window listening to popup window
    // errors
    this.closeChecker = interval(500)
    .pipe(
      switchMap(number => {
        if (this.popupWindow.closed) {
          this.handleUnexpectedWindowClosed();
          observableThrowError(new Error());
        }
        return of(null);
      }),
      filter(event => event !== null),
    );

    // Only take the first thing that comes to us, so we don't need a bunch of boolean flags everywhere
    return merge(
      this.gotNewInfo.asObservable(),
      this.closeChecker
    )
    .pipe(take(1));
  }

  closePaypalWindow() {
    if (this.popupWindow) {
      this.popupWindow.close();
    } else {
      window.close();
    }
    this.paypalWindowClosed();
    this.popupWindow = null;
  }

  /**
   * Extend this function to add functionality around whenever the paypal window gets new data
   * @param data paypal window data
   */
  protected handleNewWindowData(data: any) {}

  /**
   * Function that you extend to do things when the paypal window closed when it should have been
   */
  protected paypalWindowClosed() {}

  /**
   * Extend this to do things when the paypal window was closed unexpectedly
   */
  protected handleUnexpectedWindowClosed() {}

  /**
   * Extend this to do things when the paypal window was blocked when we tried to open it
   */
  protected handlePopupsBlocked() {}

  protected getPaypalWindowData(event) {
    const data = JSON.parse(event.data);
    this.handleNewWindowData(data);
    this.gotNewInfo.next(data);
  }

  sendPaypalInfo(json_blob) {
    window.opener.postMessage(JSON.stringify(json_blob));
  }

  private popupsAreBlocked() {
    try {
      this.popupWindow.focus();
      return !this.popupWindow || this.popupWindow.closed || typeof this.popupWindow.closed === 'undefined';
    } catch (event) {
      return true;
    }
  }

  private getPopupWindowKwargsString() {
    // Fixes dual-screen position                            Most browsers      Firefox
    const dualScreenLeft = window.screenLeft !== undefined ? window.screenLeft : 0;
    const dualScreenTop = window.screenTop !== undefined ? window.screenTop : 0;

    const width = window.innerWidth ?
      window.innerWidth : document.documentElement.clientWidth ?
        document.documentElement.clientWidth : screen.width;
    const height = window.innerHeight ?
      window.innerHeight : document.documentElement.clientHeight ?
        document.documentElement.clientHeight : screen.height;

    const left = ((width / 2) - (800 / 2)) + dualScreenLeft;
    const top = ((height / 2) - (600 / 2)) + dualScreenTop;
    return 'scrollbars=yes,menubar=0,toolbar=0,width=' + 800 + ', height=' + 600 + ', top=' + top + ', left=' + left;
  }
}
