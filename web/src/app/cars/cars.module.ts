import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CarsRoutingModule } from './cars-routing.module';
import { CreateCarFormComponent } from './forms/create-car-form/create-car-form.component';
import { CarListPageComponent } from './pages/car-list-page/car-list-page.component';
import { BaseModule } from '../base/base.module';
import { AddCarPageComponent } from './pages/add-car-page/add-car-page.component';
import { CallbackPageComponent } from './pages/callback-page/callback-page.component';

@NgModule({
  declarations: [CreateCarFormComponent, CarListPageComponent, AddCarPageComponent, CallbackPageComponent],
  imports: [
    CommonModule,
    CarsRoutingModule,
    BaseModule
  ]
})
export class CarsModule { }
