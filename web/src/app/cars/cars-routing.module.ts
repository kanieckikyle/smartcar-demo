import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CarListPageComponent } from './pages/car-list-page/car-list-page.component';
import { AddCarPageComponent } from './pages/add-car-page/add-car-page.component';
import { CallbackPageComponent } from './pages/callback-page/callback-page.component';

const routes: Routes = [
  {
    path: '',
    component: CarListPageComponent
  },
  {
    path: 'add',
    component: AddCarPageComponent
  },
  {
    path: 'callback',
    component: CallbackPageComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CarsRoutingModule { }
