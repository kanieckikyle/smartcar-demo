import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { BehaviorSubject, Observable } from 'rxjs';
import { tap, switchMap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private helper = new JwtHelperService();
  private loggedIn: BehaviorSubject<boolean> = new BehaviorSubject(localStorage.getItem('token') !== undefined);

  constructor(private http: HttpClient) { }

  login(data: any) {
    return this.http
      .post(`${environment.baseUrl}/auth/token/`, data)
      .pipe(
        tap((response: any) => {
          const token = response.token;
          localStorage.setItem('token', token);
          this.loggedIn.next(true);
          return this.helper.decodeToken(token);
        })
      );
  }

  logout() {
    localStorage.removeItem('token');
    this.loggedIn.next(false);
  }

  register(data: any) {
    return this.http
      .post(`${environment.baseUrl}/register/`, data)
      .pipe(
        switchMap( _ => this.login({username: data.username, password: data.password}))
      );
  }

  get currentUser(): JwtResponse {
    return this.helper.decodeToken(localStorage.getItem('token'));
  }

  get isLoggedIn(): boolean {
    const token = localStorage.getItem('token');
    if (token) {
      return this.helper.isTokenExpired(token);
    }
    return false;
  }

  get isLoggedIn$(): Observable<boolean> {
    return this.loggedIn.asObservable();
  }
}
