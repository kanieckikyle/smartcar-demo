import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AccountsRoutingModule } from './accounts-routing.module';
import { LoginFormComponent } from './forms/login-form/login-form.component';
import { LoginPageComponent } from './pages/login-page/login-page.component';
import { BaseModule } from '../base/base.module';
import { RegisterPageComponent } from './pages/register-page/register-page.component';
import { RegisterFormComponent } from './forms/register-form/register-form.component';

@NgModule({
  declarations: [
    LoginFormComponent,
    LoginPageComponent,
    RegisterPageComponent,
    RegisterFormComponent
  ],
  imports: [
    CommonModule,
    AccountsRoutingModule,
    BaseModule,
  ]
})
export class AccountsModule { }
