import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/accounts/services/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-base-page',
  templateUrl: './base-page.component.html',
  styleUrls: ['./base-page.component.scss']
})
export class BasePageComponent implements OnInit {

  constructor(private authService: AuthenticationService, private router: Router) { }

  ngOnInit() {

    if (this.authService.isLoggedIn) {
      if (this.authService.currentUser.is_staff) {
        this.router.navigate(['/cars']);
      } else {
        this.router.navigate(['/cars']);
      }
    }
  }

}
