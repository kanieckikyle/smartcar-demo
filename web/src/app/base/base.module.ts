import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import {MatStepperModule} from '@angular/material/stepper';
import {MatListModule} from '@angular/material/list';

import { BaseRoutingModule } from './base-routing.module';
import { NavbarComponent } from './elements/navbar/navbar.component';
import { BasePageComponent } from './pages/base-page/base-page.component';
import { VarDirective } from './directives/ng-var.directive';


@NgModule({
  declarations: [
    NavbarComponent,
    BasePageComponent,
    VarDirective
  ],
  imports: [
    CommonModule,
    BaseRoutingModule,
    MatFormFieldModule,
    MatInputModule,
    MatToolbarModule,
    MatButtonModule,
    ReactiveFormsModule,
    MatCardModule,
    MatStepperModule,
    MatListModule
  ],
  exports: [
    MatFormFieldModule,
    MatInputModule,
    MatToolbarModule,
    MatButtonModule,
    NavbarComponent,
    ReactiveFormsModule,
    MatCardModule,
    MatStepperModule,
    MatListModule,
    VarDirective
  ]
})
export class BaseModule { }
