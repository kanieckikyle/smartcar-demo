from rest_framework import serializers
from jobs.models import JobType


class JobTypeSerializer(serializers.ModelSerializer):

    class Meta:
        model = JobType
        fields = (
            'name'
        )
