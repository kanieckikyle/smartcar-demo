from rest_framework import serializers
from jobs.models import Job

from accounts.serializers import OwnerSerializer
from cars.serializers import CarSerializer


class JobSerializer(serializers.ModelSerializer):

    type = serializers.StringRelatedField()
    mechanic = OwnerSerializer()
    car = CarSerializer()

    class Meta:
        model = Job
        fields = (
            'id',
            'car',
            'mechanic',
            'type',
            'quote',
            'pickup_time',
            'duration',
            'picked_up',
            'completed'
        )
