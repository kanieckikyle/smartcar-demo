from django.contrib import admin

from jobs.models import Job, JobType


class JobTypeAdminConsole(admin.ModelAdmin):
    pass


class JobAdminConsole(admin.ModelAdmin):
    list_display = ('id', 'car', 'mechanic', 'type')


admin.site.register(JobType, JobTypeAdminConsole)
admin.site.register(Job, JobAdminConsole)
