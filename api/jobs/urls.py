from django.urls import path

from jobs.views import JobList

urlpatterns = [
    path('', JobList.as_view(), name="JobList")
]
