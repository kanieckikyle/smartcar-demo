from rest_framework.views import APIView, Response
from rest_framework import status
from django.db.models import Q

from jobs.models import Job
from jobs.serializers import JobSerializer
from api.utils import prepare_request_query_params


class JobList(APIView):

    def get(self, request, format=None):
        filters = self.get_filter(request)
        jobs = Job.objects.prefetch_related('mechanic', 'car', 'type').filter(filters)
        if not request.user.is_superuser:
            jobs = jobs.filter(car__owner=request.user)

        serializer = JobSerializer(jobs, many=True)
        return Response(data=serializer.data, status=status.HTTP_200_OK)

    def post(self, request, format=None):
        serializer = JobSerializer(data=request.data)

        if serializer.is_valid():
            job = serializer.create(serializer.validated_data)
            return Response(data=JobSerializer(job).data, status=status.HTTP_200_OK)

        return Response(data=serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_filter(self, request):
        filter = Q()
        query_params = prepare_request_query_params(request)
        if 'completed' in query_params:
            filter &= Q(completed=query_params.get('completed'))

        if 'picked_up' in query_params:
            filter &= Q(picked_up=query_params.get('picked_up'))

        if 'completed' in query_params:
            filter &= Q(completed=query_params.get('completed'))

        return filter
