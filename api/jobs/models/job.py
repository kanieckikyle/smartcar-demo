from django.db import models

from accounts.models import Owner
from jobs.models import JobType
from cars.models import Car


class Job(models.Model):

    car = models.ForeignKey(Car, related_name='jobs', on_delete=models.CASCADE, null=False, blank=False)

    mechanic = models.ForeignKey(Owner, related_name='mechanic_jobs', on_delete=models.SET_NULL, null=True, blank=True)

    type = models.ForeignKey(JobType, related_name='jobs', on_delete=models.SET_NULL, null=True, blank=False)

    quote = models.PositiveIntegerField(null=True, blank=True)

    pickup_time = models.DateTimeField(null=True, blank=False)

    duration = models.DurationField(blank=True, null=True)

    picked_up = models.BooleanField(default=False)

    completed = models.BooleanField(default=False)
