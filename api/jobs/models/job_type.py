from django.db import models


class JobType(models.Model):
    name = models.CharField(max_length=40, blank=False, null=False)

    def __str__(self):
        return self.name
