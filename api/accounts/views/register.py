from rest_framework.views import APIView, Response
from rest_framework import status, permissions

from accounts.serializers import RegisterSerializer, OwnerSerializer


class Register(APIView):

    permission_classes = (permissions.AllowAny, )

    def post(self, request, format=None):
        serializer = RegisterSerializer(data=request.data)

        if serializer.is_valid():
            owner = serializer.create(serializer.validated_data)
            return Response(data=OwnerSerializer(owner).data, status=status.HTTP_200_OK)

        return Response(data=serializer.errors, status=status.HTTP_400_BAD_REQUEST)
