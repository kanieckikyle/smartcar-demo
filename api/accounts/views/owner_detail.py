from rest_framework.views import APIView, Response
from rest_framework import status, permissions
from django.http import Http404

from accounts.models import Owner
from accounts.serializers import OwnerSerializer


class OwnerDetail(APIView):

    permission_classes = (permissions.AllowAny, )

    def get(self, request, id, format=None):
        owner = self.get_owner(id)
        return Response(data=OwnerSerializer(owner).data, status=status.HTTP_200_OK)

    def patch(self, request, id, format=None):
        owner = self.get_owner(id)
        serializer = OwnerSerializer(instance=owner, data=request.data, partial=True)

        if serializer.is_valid():
            serializer.update(owner, serializer.validated_data)
            return Response(data=serializer.validated_data, status=status.HTTP_200_OK)

        return Response(status=status.HTTP_400_BAD_REQUEST)

    def get_owner(self, id):
        try:
            return Owner.objects.get(id=id)
        except Owner.DoesNotExist:
            raise Http404()
