from .owner_detail import OwnerDetail
from .register import Register

__all__ = [
    'OwnerDetail',
    'Register'
]
