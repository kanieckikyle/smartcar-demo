from accounts.serializers import JwtSerializer

def jwt_payload_handler(user):
    return JwtSerializer(user).data
