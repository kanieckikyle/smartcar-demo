from django.urls import path
from rest_framework_jwt.views import obtain_jwt_token, verify_jwt_token
from accounts.views import OwnerDetail, Register

urlpatterns = [
    path('auth/token/', obtain_jwt_token, name="login"),
    path('auth/token/verify/', verify_jwt_token, name="verify"),
    path('register/', Register.as_view(), name="register"),
    path('owners/<int:id>/', OwnerDetail.as_view(), name="OwnerDetail")
]
