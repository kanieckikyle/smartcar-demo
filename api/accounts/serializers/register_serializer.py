from rest_framework import serializers

from accounts.models import Owner


class RegisterSerializer(serializers.ModelSerializer):

    class Meta:
        model = Owner
        fields = (
            'username',
            'email',
            'password',
            'first_name',
            'last_name'
        )

    def create(self, validated_data):
        owner = super().create(validated_data)
        owner.set_password(validated_data.get('password', '123qweasd'))
        owner.save()
        return owner
