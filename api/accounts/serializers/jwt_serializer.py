from rest_framework import serializers
from calendar import timegm
from datetime import datetime
from rest_framework_jwt.settings import api_settings

from accounts.models import Owner


class JwtSerializer(serializers.ModelSerializer):
    exp = serializers.SerializerMethodField()
    orig_iat = serializers.SerializerMethodField()

    class Meta:
        model = Owner
        fields = (
            'id',
            'username',
            'email',
            'first_name',
            'last_name',
            'is_superuser',
            'is_staff',
            'is_active',
            'exp',
            'orig_iat'
        )

    def get_exp(self, obj):
        return datetime.utcnow() + api_settings.JWT_EXPIRATION_DELTA

    def get_orig_iat(self, obj):
        return timegm(datetime.utcnow().utctimetuple())
