from rest_framework import serializers

from accounts.models import Owner


class OwnerSerializer(serializers.ModelSerializer):

    class Meta:
        model = Owner
        fields = (
            'id',
            'username',
            'email',
            'first_name',
            'last_name'
        )
