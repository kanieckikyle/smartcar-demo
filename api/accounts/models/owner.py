from django.db import models
from django.contrib.auth.models import AbstractUser


class Owner(AbstractUser):

    smartcar_access = models.CharField(max_length=30, null=True, editable=False)

    smartcar_refresh = models.CharField(max_length=50, null=True, editable=False)

    def __str__(self):
        return self.username
