from django.contrib import admin

from accounts.models import Owner


class OwnerAdminConsole(admin.ModelAdmin):
    list_display = ('id', 'username', 'email')


admin.site.register(Owner, OwnerAdminConsole)
