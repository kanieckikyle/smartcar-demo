from rest_framework import serializers

from cars.models import Car
from accounts.serializers import OwnerSerializer


class CarSerializer(serializers.ModelSerializer):

    owner = OwnerSerializer()

    class Meta:
        model = Car
        fields = (
            'id',
            'owner',
            'make',
            'model',
            'year',
            'color'
        )

        read_only_fields = ('id', 'owner', 'make', 'model', 'year')

    def create(self, validated_data, owner):
        validated_data['owner'] = owner
        return super().create(validated_data)
