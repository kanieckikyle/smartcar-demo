from django.db import models

from accounts.models import Owner


class Car(models.Model):

    owner = models.ForeignKey(Owner, on_delete=models.CASCADE, related_name='cars', null=False, blank=False)

    vehicle_id = models.UUIDField(null=True, editable=False)

    make = models.CharField(max_length=30, null=False, blank=False)

    model = models.CharField(max_length=50, null=False, blank=False)

    year = models.CharField(max_length=4, null=False, blank=False)

    color = models.CharField(max_length=15, null=False, blank=False)

    def __str__(self):
        return '{}\'s {} {}'.format(self.owner.username, self.make, self.model)
