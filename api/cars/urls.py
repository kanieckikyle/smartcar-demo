from django.urls import path

from .views import CarList, Smartcar, CarDetail

urlpatterns = [
    path('', CarList.as_view(), name="CarList"),
    path('authorize/', Smartcar.as_view(), name="Smartcar"),
    path('<int:id>/', CarDetail.as_view(), name="CarDetail")
]
