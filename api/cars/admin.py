from django.contrib import admin

from cars.models import Car


class CarAdminConsole(admin.ModelAdmin):
    list_display = ('id', 'owner', 'make', 'model', 'year')


admin.site.register(Car, CarAdminConsole)
