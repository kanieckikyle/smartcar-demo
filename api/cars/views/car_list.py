from rest_framework.views import APIView, Response
from rest_framework import status

from cars.models import Car
from cars.serializers import CarSerializer


class CarList(APIView):

    def get(self, request, format=None):
        cars = self.get_cars(request.user)
        serializer = CarSerializer(cars, many=True)
        return Response(data=serializer.data, status=status.HTTP_200_OK)

    def get_cars(self, owner):
        if owner.is_superuser:
            return Car.objects.all()
        return owner.cars
