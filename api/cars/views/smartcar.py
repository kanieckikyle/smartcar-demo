from rest_framework.views import APIView, Response
from rest_framework import status
from django.conf import settings

import smartcar
from cars.models import Car
from cars.serializers import CarSerializer


class Smartcar(APIView):

    def get(self, request, format=None):
        client = self.get_auth_client()
        redirect_url = client.get_auth_url(force=True)
        return Response(data={'redirect_url': redirect_url}, status=status.HTTP_200_OK)

    def post(self, request, format=None):
        code = request.data.get('code', '')
        if code:
            client = self.get_auth_client()
            access_body = client.exchange_code(code)
            user = request.user
            user.smartcar_access = access_body['access_token']
            user.smartcar_refresh = access_body['refresh_token']
            user.save()
            cars = self.get_user_cars(user)
            return Response(
                data=CarSerializer(cars, many=True).data,
                status=status.HTTP_200_OK
            )

        return Response(status=status.HTTP_400_BAD_REQUEST)

    def get_auth_client(self):
        if settings.DEBUG:
            redirect = 'http://localhost:4200/cars/callback/'
        else:
            redirect = ''

        return smartcar.AuthClient(
            settings.SMARTCAR_CLIENT_ID,
            settings.SMARTCAR_CLIENT_SECRET,
            redirect,
            test_mode=settings.DEBUG
        )

    def get_user_cars(self, user):
        access_token = user.smartcar_access
        vehicle_ids = smartcar.get_vehicle_ids(user.smartcar_access)['vehicles']
        cars = []
        for id in vehicle_ids:
            print(id)
            vehicle = smartcar.Vehicle(id, access_token)
            print(vehicle)
            info = self.transform_smartcar_info(vehicle, user)
            try:
                car = Car.objects.get(**info)
            except Car.DoesNotExist:
                car = Car.objects.create(**info)
            cars.append(car)
        return cars

    def transform_smartcar_info(self, vehicle, user):
        info = vehicle.info()
        info['vehicle_id'] = info['id']
        del info['id']
        info['owner'] = user
        return info
