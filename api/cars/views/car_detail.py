from rest_framework.views import APIView, Response
from rest_framework import status
from django.exceptions import Http404

from cars.models import Car
from cars.serializers import CarSerializer


class CarDetail(APIView):

    def get(self, request, id, format=None):
        pass

    def patch(self, request, id, format=None):
        car = self.get_car(id, request.user)
        serializer = CarSerializer(car, partial=True, data=request.data)

        if serializer.is_valid():
            car = serializer.update(car, serializer.validated_data)
            return Response(data=CarSerializer(car).data, status=status.HTTP_200_OK)

        return Response(data=serializer.errors, status=status.HTTP_400_BAD_RESPONSE)

    def get_car(self, id, owner):
        try:
            return Car.objects.get(id=id, owner=owner)
        except Car.DoesNotExist:
            raise Http404()
