def prepare_request_query_params(request):
    params = {}
    for (key, value) in request.GET:
        if value == 'true' or value == 'True':
            params[key] = True
        elif value == 'false' or value == 'False':
            params[key] = False
        else:
            params[key] = value

    return params
